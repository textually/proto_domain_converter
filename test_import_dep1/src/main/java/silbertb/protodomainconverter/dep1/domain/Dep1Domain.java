package silbertb.protodomainconverter.dep1.domain;

import lombok.Builder;
import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoEnum;
import org.silbertb.proto.domainconverter.model.proto.Dep1Proto;

@Builder
@Value
@ProtoBuilder
@ProtoClass(protoClass = Dep1Proto.class, blacklist = true)
public class Dep1Domain {
    @ProtoEnum(protoEnum = Dep1Proto.Dep1Enum.class)
    public enum Dep1Enum {
        E1,
        E2
    }
    String stringValue;
    Dep1Enum enumValue;
}
