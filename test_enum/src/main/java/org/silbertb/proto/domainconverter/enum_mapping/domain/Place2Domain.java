package org.silbertb.proto.domainconverter.enum_mapping.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClassDefault;
import org.silbertb.proto.domainconverter.annotations.ProtoEnum;
import org.silbertb.proto.domainconverter.test.proto.enummapping.PlaceProto;

@ProtoClassDefault
@ProtoEnum(protoEnum = PlaceProto.class)
public enum Place2Domain {
    HERE,
    THERE
}