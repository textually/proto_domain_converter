package org.silbertb.proto.domainconverter.enum_mapping.domain;

import lombok.Builder;
import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoEnum;
import org.silbertb.proto.domainconverter.test.proto.enummapping.ClassWithEnumProto;

@Value
@Builder
@ProtoBuilder
@ProtoClass(protoClass = ClassWithEnumProto.class, blacklist = true)
public class ClassWithEnumDomain {
    @ProtoEnum(protoEnum = ClassWithEnumProto.ColorProto.class)
    public enum ColorDomain {
        BLUE,
        RED
    }

    ColorDomain color;
    EnumDomain e;
}
