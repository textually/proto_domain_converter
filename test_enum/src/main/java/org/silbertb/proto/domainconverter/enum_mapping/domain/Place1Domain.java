package org.silbertb.proto.domainconverter.enum_mapping.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoEnum;
import org.silbertb.proto.domainconverter.test.proto.enummapping.PlaceProto;

@ProtoEnum(protoEnum = PlaceProto.class)
public enum Place1Domain {
    HERE,
    THERE
}
