package org.silbertb.proto.domainconverter.custom;

@SuppressWarnings("rawtypes")
public class NullConverter implements TypeConverter {
    @Override
    public Object toDomainValue(Object protoValue) {
        return null;
    }

    @Override
    public boolean shouldAssignToProto(Object domainValue) {
        return false;
    }

    @Override
    public Object toProtobufValue(Object domainValue) {
        return null;
    }
}
