package org.silbertb.proto.domainconverter.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Map between a base class or interface to oneof group
 * Used with conjunction fo {@link OneofField}
 */
@Target({ElementType.FIELD, ElementType.TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.SOURCE)
public @interface OneofBase {
    /**
     * The name of the oneof definition in the protobuf schema.
     * If the annotation is attached to a field the default is derived from the field name.
     * If the annotation is attached to the class then it must be specified.
     * @return Protobuf oneof definition name
     */
    String oneofName() default "";

    /**
     * A list of annotations which map classes derived from the annotated class and their domain field name to corresponding protobuf fields in the oneof group.
     * @return A list of {@link OneofField} annotations
     */
    OneofField[] oneOfFields();
}
