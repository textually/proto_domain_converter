package org.silbertb.proto.domainconverter.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Place this annotation only above class which implements {@link org.silbertb.proto.domainconverter.custom.Mapper}.
 * This annotation will generate "toProto" and "toDomain" just like {@link ProtoClassMapper} combined with {@link ProtoClass}.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoGlobalMapper {
}
