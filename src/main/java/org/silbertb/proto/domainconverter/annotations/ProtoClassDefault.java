package org.silbertb.proto.domainconverter.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * In case there are multiple domain classes with the same proto class,
 * this annotation can be used to specify the default domain class.
 * If multiple domain classes are annotated with this annotation,
 * compilation will fail.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoClassDefault {
}
