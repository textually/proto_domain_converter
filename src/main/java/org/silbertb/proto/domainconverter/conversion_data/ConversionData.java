package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.silbertb.proto.domainconverter.util.StringUtils;

import java.util.Collection;
import java.util.List;

@Accessors(fluent = true)
@Getter
public class ConversionData {
    private final ConfigurationData configurationData;
    private final String generator;
    private final String converterPackage;
    private final String converterClass;
    private final List<ClassData> classesData;
    private final Collection<ClassData> defaultClassesData;
    private final Collection<EnumData> defaultEnumsData;
    private final Collection<EnumData> enumData;

    @Builder
    private ConversionData(ConfigurationData configurationData,
                           String generator,
                           String converterFullName,
                           List<ClassData> classesData,
                           Collection<ClassData> defaultClassesData,
                           Collection<EnumData> enumData,
                           Collection<EnumData> defaultEnumsData) {
        this.configurationData = configurationData;
        this.generator = generator;
        this.converterPackage = StringUtils.getPackage(converterFullName);
        this.converterClass = StringUtils.getSimpleName(converterFullName);
        this.classesData = classesData;
        this.defaultClassesData = defaultClassesData;
        this.enumData = enumData;
        this.defaultEnumsData = defaultEnumsData;
    }
}
