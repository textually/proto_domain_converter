package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Value;
import lombok.experimental.Accessors;

import java.util.Map;
import java.util.Set;


@Accessors(fluent = true)
@Value
public class ConfigurationData {
    String generatedConverterName;
    Map<String, String> domainClassToConverter;
    Set<String> directImports;
    Set<String> transitiveImports;

    public String getConverterName(String domainTypeFullName) {
        return domainClassToConverter.get(domainTypeFullName);
    }
}
