package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Getter
@Builder
public class BuilderData {
    private String builderMethodName;
    private String buildMethodName;
    private String setterPrefix;
    private boolean useConstructorParams;

    public boolean hasSetterPrefix() {
        return !setterPrefix.isEmpty();
    }
}
