package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.ProtoClassDefault;
import org.silbertb.proto.domainconverter.custom.Mapper;
import org.silbertb.proto.domainconverter.util.LangModelUtil;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.List;

public class GlobalMapperTypesCreator {
    private final LangModelUtil langModelUtil;

    public GlobalMapperTypesCreator(LangModelUtil langModelUtil) {
        this.langModelUtil = langModelUtil;
    }

    public GlobalMapperInfo create(TypeElement globalMapper) {
        String mapperFullName = globalMapper.getQualifiedName().toString();

        TypeMirror globalMapperType = globalMapper.asType();
        if(!langModelUtil.isAssignedFrom(globalMapperType, Mapper.class)) {
            throw new RuntimeException(mapperFullName + " is annotated with '@ProtoGlobalMapper' but not implements 'Mapper'");
        }

        TypeMirror mapperInterface = langModelUtil.getInterfaceOf(globalMapperType, Mapper.class.getName());

        List<? extends TypeMirror> genericTypes = langModelUtil.getGenericsTypes(mapperInterface);

        String domainFullName = genericTypes.get(0).toString();
        String protoFullName = genericTypes.get(1).toString();

        ProtoClassDefault protoClassDefaultAnnotation = globalMapper.getAnnotation(ProtoClassDefault.class);
        boolean isDefault = protoClassDefaultAnnotation != null;

        return new GlobalMapperInfo(mapperFullName, domainFullName, protoFullName, isDefault);
    }
}
