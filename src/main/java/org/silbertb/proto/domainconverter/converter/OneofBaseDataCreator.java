package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.conversion_data.*;
import org.silbertb.proto.domainconverter.converter.oneof_field.OneofFieldDataCreator;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;
import org.silbertb.proto.domainconverter.util.StringUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OneofBaseDataCreator {

    private final LangModelUtil langModelUtil;
    private final ProtoTypeUtil protoTypeUtil;
    private final ProcessingEnvironment processingEnv;
    private final ClassDataCreator classDataCreator;
    private ConstructorParametersDataCreator constructorParametersDataCreator;
    private BuilderDataCreator builderDataCreator;
    private FieldDataCreator fieldDataCreator;
    private final ConverterLogger logger;

    public OneofBaseDataCreator(LangModelUtil langModelUtil, ProtoTypeUtil protoTypeUtil, ProcessingEnvironment processingEnv, ConverterLogger logger,
                                ClassDataCreator classDataCreator) {
        this.langModelUtil = langModelUtil;
        this.protoTypeUtil = protoTypeUtil;
        this.processingEnv = processingEnv;
        this.classDataCreator = classDataCreator;
        this.logger = logger;
    }

    public void setConstructorParametersDataCreator(ConstructorParametersDataCreator constructorParametersDataCreator) {
        this.constructorParametersDataCreator = constructorParametersDataCreator;
    }

    public void setBuilderDataCreator(BuilderDataCreator builderDataCreator) {
        this.builderDataCreator = builderDataCreator;
    }

    public void setFieldDataCreator(FieldDataCreator fieldDataCreator) {
        this.fieldDataCreator = fieldDataCreator;
    }

    public OneofBaseFieldData createOneofBaseFieldData(VariableElement field) {
        OneofBase oneofBaseAnnotation = field.getAnnotation(OneofBase.class);
        if(oneofBaseAnnotation == null) {
            return null;
        }

        String domainFieldName = field.getSimpleName().toString();
        String oneofBaseField = StringUtils.capitalize(field.getSimpleName().toString());
        OneofBaseFieldData.OneofBaseFieldDataBuilder oneofBaseFieldDataBuilder = OneofBaseFieldData.builder();
        oneofBaseFieldDataBuilder
                .oneofProtoName(
                        oneofBaseAnnotation.oneofName().equals("") ?
                                StringUtils.capitalize(field.getSimpleName().toString()) :
                                StringUtils.snakeCaseToPascalCase(oneofBaseAnnotation.oneofName()))
                .domainFieldName(domainFieldName)
                .oneofBaseField(oneofBaseField)
                .oneOfFieldsData(createOneofFieldDataList(oneofBaseAnnotation, oneofBaseField))
                .domainFieldType(field.asType().toString());

        return oneofBaseFieldDataBuilder.build();
    }

    public OneofBaseClassData createOneofBaseClassData(OneofBase oneofBaseAnnotation, TypeMirror protoClass, boolean blacklist) {
        if(oneofBaseAnnotation == null) {
            return null;
        }

        return OneofBaseClassData.builder()
                .oneofProtoName(StringUtils.snakeCaseToPascalCase(oneofBaseAnnotation.oneofName()))
                .oneOfFieldsData(createOneofFieldDataForClassList(oneofBaseAnnotation, protoClass, blacklist))
                .build();
    }

    private List<OneofFieldDataForClass> createOneofFieldDataForClassList(OneofBase oneofBaseAnnotation, TypeMirror protoClass, boolean blacklist) {
        OneofFieldComparator oneofFieldComparator = new OneofFieldComparator(processingEnv, langModelUtil);
        return Arrays.stream(oneofBaseAnnotation.oneOfFields())
                .sorted(oneofFieldComparator)
                .map(oneofField -> createOneofFieldDataForClass(oneofField, oneofFieldComparator.getType(oneofField), protoClass, blacklist))
                .collect(Collectors.toList());
    }

    private OneofFieldDataForClass createOneofFieldDataForClass(OneofField oneofFieldAnnotation, TypeMirror domainType, TypeMirror protoClass, boolean blacklist) {
        TypeElement domainElement = (TypeElement)processingEnv.getTypeUtils().asElement(domainType);
        ProtoClass oneofBaseAnnotation = domainElement.getAnnotation(ProtoClass.class);
        boolean isMessage = oneofBaseAnnotation != null;

        ClassData classData = isMessage ? classDataCreator.createClassData(domainElement) :
                classDataCreator.createClassData(domainElement, protoClass, true, blacklist);


        String protoField = oneofFieldAnnotation.protoField();

        return OneofFieldDataForClass.builder()
                .protoField(protoField)
                .classData(classData)
                .isMessage(isMessage)
                .build();
    }

    private List<OneofFieldData> createOneofFieldDataList(OneofBase oneofBaseAnnotation, String oneofBaseField) {
        OneofFieldDataCreator oneofFieldDataCreator = new OneofFieldDataCreator(
                langModelUtil, protoTypeUtil, processingEnv, builderDataCreator, fieldDataCreator, constructorParametersDataCreator);
        OneofFieldComparator oneofFieldComparator = new OneofFieldComparator(processingEnv, langModelUtil);
        return Arrays.stream(oneofBaseAnnotation.oneOfFields())
                .sorted(oneofFieldComparator)
                .map(oneofField -> oneofFieldDataCreator.create(oneofField, oneofFieldComparator.getType(oneofField), oneofBaseField))
                .collect(Collectors.toList());
    }
}
