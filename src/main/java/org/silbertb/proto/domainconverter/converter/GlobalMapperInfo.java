package org.silbertb.proto.domainconverter.converter;

import lombok.Value;

@Value
public class GlobalMapperInfo {
    String mapperName;
    String domainName;
    String protoName;

    boolean isDefault;
}
