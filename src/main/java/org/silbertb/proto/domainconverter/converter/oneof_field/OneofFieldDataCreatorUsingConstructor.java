package org.silbertb.proto.domainconverter.converter.oneof_field;

import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.conversion_data.BuilderData;
import org.silbertb.proto.domainconverter.conversion_data.ConcreteFieldData;
import org.silbertb.proto.domainconverter.conversion_data.FieldData;
import org.silbertb.proto.domainconverter.conversion_data.OneofFieldData;
import org.silbertb.proto.domainconverter.converter.BuilderDataCreator;
import org.silbertb.proto.domainconverter.converter.ConstructorParametersDataCreator;
import org.silbertb.proto.domainconverter.custom.ProtoType;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.StringUtils;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.List;

public class OneofFieldDataCreatorUsingConstructor {

    private final ConstructorParametersDataCreator constructorParametersDataCreator;
    private final BuilderDataCreator builderDataCreator;
    private final OneofConverterUtil oneofConverterUtil;
    private final LangModelUtil langModelUtil;

    public OneofFieldDataCreatorUsingConstructor(ConstructorParametersDataCreator constructorParametersDataCreator,
                                                 BuilderDataCreator builderDataCreator,
                                                 OneofConverterUtil oneofConverterUtil,
                                                 LangModelUtil langModelUtil) {
        this.constructorParametersDataCreator = constructorParametersDataCreator;
        this.builderDataCreator = builderDataCreator;
        this.oneofConverterUtil = oneofConverterUtil;
        this.langModelUtil = langModelUtil;
    }

    public OneofFieldData create(OneofField oneofFieldAnnotation, TypeElement domainTypeElement) {
        List<FieldData> constructorParametersData =
                constructorParametersDataCreator.getConstructorParametersData(domainTypeElement, ProtoConstructor.class, Modifier.PUBLIC);

        BuilderData builderData = builderDataCreator.getConstructorBuilderData(domainTypeElement);
        if(builderData != null) {
            if(constructorParametersData != null) {
                throw new RuntimeException(domainTypeElement + " has both @ProtoConstructor and @ProtoBuilder defined");
            }

            constructorParametersData = builderDataCreator.createBuilderConstructorParametersData(domainTypeElement);
        }

        if(constructorParametersData == null) {
            return null;
        }

        if (constructorParametersData.size() != 1) {
            throw new RuntimeException("There should be only one constructor parameter for " + domainTypeElement);
        }

        FieldData param = constructorParametersData.get(0);
        if (param.oneofFieldData() != null) {
            throw new RuntimeException("There shouldn't be constructor parameter for " + domainTypeElement +
                    " annotated with @OneofBase since this class is oneof implementation itself.");
        }

        if (oneofFieldAnnotation.domainField().isEmpty() &&
                !param.concreteFieldData().protoFieldPascalCase().equals(
                        StringUtils.snakeCaseToPascalCase(oneofFieldAnnotation.protoField()))) {
            throw new RuntimeException("Constructor parameter for " + domainTypeElement + " should correlate proto field " + oneofFieldAnnotation.protoField());
        }

        if (!oneofFieldAnnotation.domainField().isEmpty()) {
            if(!param.concreteFieldData().domainFieldName().equals(oneofFieldAnnotation.domainField())) {
                throw new RuntimeException("Constructor parameter for " + domainTypeElement + " should correlate domain field " + oneofFieldAnnotation.protoField());
            }

            @SuppressWarnings("ResultOfMethodCallIgnored")
            TypeMirror converterType = langModelUtil.getClassFromAnnotation(oneofFieldAnnotation::converter);
            if(oneofConverterUtil.isNullConverter(converterType)) {
                converterType = null;
            } else {
                oneofConverterUtil.validateConverter(oneofFieldAnnotation, converterType);
            }
            param = getUpdatedFieldData(param, converterType, oneofFieldAnnotation.protoField());

        }

        ConcreteFieldData constructorParameter = param.concreteFieldData();

        return OneofFieldData.builder()
                .protoField(oneofFieldAnnotation.protoField())
                .oneofImplClass(domainTypeElement.getQualifiedName().toString())
                .constructorParameter(constructorParameter)
                .builderData(builderData)
                .build();
    }

    private FieldData getUpdatedFieldData(FieldData fieldData, TypeMirror converter, String protoField) {
        ConcreteFieldData concreteFieldData =
                getUpdatedConcreteFieldData(fieldData.concreteFieldData(), converter, protoField);

        return new FieldData(concreteFieldData, null, fieldData.isLast());
    }

    private ConcreteFieldData getUpdatedConcreteFieldData(ConcreteFieldData concreteFieldData, TypeMirror converter, String protoField) {
        ConcreteFieldData.ConcreteFieldDataBuilder concreteFieldDataBuilder = concreteFieldData
                .toBuilder()
                .protoTypeForConverter(ProtoType.OTHER)
                .explicitProtoFieldName(protoField);

        if(converter != null) {
            concreteFieldDataBuilder.converterFullName(converter.toString());
        }

        return concreteFieldDataBuilder.build();
    }
}
