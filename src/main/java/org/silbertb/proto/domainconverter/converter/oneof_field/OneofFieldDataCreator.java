package org.silbertb.proto.domainconverter.converter.oneof_field;

import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.conversion_data.OneofFieldData;
import org.silbertb.proto.domainconverter.converter.BuilderDataCreator;
import org.silbertb.proto.domainconverter.converter.ConstructorParametersDataCreator;
import org.silbertb.proto.domainconverter.converter.FieldDataCreator;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

public class OneofFieldDataCreator {

    private final ProcessingEnvironment processingEnv;
    private final OneofFieldDataCreatorUsingProtoClass creatorUsingProtoClass;
    private final OneofFieldDataCreatorUsingClassConverter creatorUsingClassConverter;
    private final OneofFieldDataCreatorUsingSettersOrClassBuilder creatorUsingSettersOrClassBuilder;
    private final OneofFieldDataCreatorUsingConstructor creatorUsingConstructorOrConstructorBuilder;

    public OneofFieldDataCreator(LangModelUtil langModelUtil,
                                 ProtoTypeUtil protoTypeUtil,
                                 ProcessingEnvironment processingEnv,
                                 BuilderDataCreator builderDataCreator,
                                 FieldDataCreator fieldDataCreator,
                                 ConstructorParametersDataCreator constructorParametersDataCreator) {
        OneofConverterUtil oneofConverterUtil = new OneofConverterUtil(langModelUtil, protoTypeUtil);
        this.processingEnv = processingEnv;
        this.creatorUsingProtoClass = new OneofFieldDataCreatorUsingProtoClass(protoTypeUtil, oneofConverterUtil);
        this.creatorUsingClassConverter = new OneofFieldDataCreatorUsingClassConverter(oneofConverterUtil);
        this.creatorUsingSettersOrClassBuilder = new OneofFieldDataCreatorUsingSettersOrClassBuilder(
                processingEnv, langModelUtil, protoTypeUtil, builderDataCreator, fieldDataCreator, oneofConverterUtil);
        this.creatorUsingConstructorOrConstructorBuilder = new OneofFieldDataCreatorUsingConstructor(
                constructorParametersDataCreator, builderDataCreator, oneofConverterUtil, langModelUtil);
    }

    public OneofFieldData create(OneofField oneofFieldAnnotation, TypeMirror domainType, String oneofBaseField) {
        TypeElement domainTypeElement = (TypeElement) processingEnv.getTypeUtils().asElement(domainType);

        OneofFieldData mapToMessage = creatorUsingProtoClass.create(oneofFieldAnnotation, domainType);
        if (mapToMessage != null) {
            return mapToMessage;
        }

        OneofFieldData toWholeClassUsingConverter = creatorUsingClassConverter.create(oneofFieldAnnotation, domainTypeElement);
        if (toWholeClassUsingConverter != null) {
            return toWholeClassUsingConverter;
        }

        OneofFieldData byConstructorOrConstructorBuilder = creatorUsingConstructorOrConstructorBuilder.create(oneofFieldAnnotation, domainTypeElement);
        OneofFieldData bySettersOrClassBuilder = creatorUsingSettersOrClassBuilder.create(oneofFieldAnnotation, domainTypeElement, oneofBaseField);

        if (byConstructorOrConstructorBuilder != null) {
            return byConstructorOrConstructorBuilder;
        }

        return bySettersOrClassBuilder;
    }
}
