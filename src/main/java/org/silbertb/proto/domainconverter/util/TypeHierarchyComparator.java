package org.silbertb.proto.domainconverter.util;

import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import java.util.Comparator;

public class TypeHierarchyComparator implements Comparator<TypeMirror> {

    private final Types types;

    public TypeHierarchyComparator(Types types) {
        this.types = types;
    }

    @Override
    public int compare(TypeMirror o1, TypeMirror o2) {
        if(types.isSameType(o1, o2)) {
            return 0;
        }

        if(types.isAssignable(o1, o2)) {
            return -1;
        }

        if(types.isAssignable(o2, o1)) {
            return 1;
        }

        return o1.toString().compareTo(o2.toString());
    }
}
