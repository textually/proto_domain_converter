package org.silbertb.proto.domainconverter.util;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoEnum;
import org.silbertb.proto.domainconverter.conversion_data.FieldType;
import org.silbertb.proto.domainconverter.custom.ProtoType;
import org.silbertb.proto.domainconverter.custom.TypeConverter;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import java.util.*;

public class ProtoTypeUtil {

    private final ProcessingEnvironment processingEnv;
    private final LangModelUtil langModelUtil;

    public ProtoTypeUtil(ProcessingEnvironment processingEnv, LangModelUtil langModelUtil) {
        this.processingEnv = processingEnv;
        this.langModelUtil = langModelUtil;
    }

    public String calculateDataStructureConcreteType(VariableElement field) {
        TypeMirror fieldType = field.asType();

        if (langModelUtil.isList(fieldType)) {
            if (langModelUtil.isConcreteType(field)) {
                return processingEnv.getTypeUtils().erasure(fieldType).toString();
            }

            return ArrayList.class.getName();
        }

        if (langModelUtil.isMap(fieldType)) {
            if (langModelUtil.isConcreteType(field)) {
                return processingEnv.getTypeUtils().erasure(fieldType).toString();
            }

            if (langModelUtil.isAssignedFrom(fieldType, SortedMap.class)) {
                return TreeMap.class.getName();
            }

            return HashMap.class.getName();
        }

        return null;
    }

    public FieldType calculateFieldType(TypeMirror fieldType) {
        return calculateFieldType(fieldType, Set.of());
    }

    public FieldType calculateFieldType(TypeMirror fieldType, Set<String> knownMessages) {
        if (fieldType.getKind().equals(TypeKind.BOOLEAN)) {
            return FieldType.BOOLEAN;
        }

        if (langModelUtil.isSameType(fieldType, String.class)) {
            return FieldType.STRING;
        }

        if (langModelUtil.isByteArray(fieldType)) {
            return FieldType.BYTES;
        }

        if(knownMessages.contains(fieldType.toString())) {
            DeclaredType declaredType = (DeclaredType) fieldType;
            return declaredType.asElement().getKind().equals(ElementKind.ENUM) ? FieldType.ENUM : FieldType.MESSAGE;
        }

        if (isProtoMessage(fieldType)) {
            return FieldType.MESSAGE;
        }

        if (isProtoEnum(fieldType)) {
            return FieldType.ENUM;
        }

        if (langModelUtil.isList(fieldType)) {
            TypeMirror typeArgument = langModelUtil.getGenericsTypes(fieldType).get(0);
            if (knownMessages.contains(typeArgument.toString()) ||
                    isProtoMessage(typeArgument) ||
                    isProtoEnum(typeArgument)) {
                return FieldType.MESSAGE_LIST;
            } else {
                return FieldType.PRIMITIVE_LIST;
            }
        }

        if (langModelUtil.isMap(fieldType)) {
            TypeMirror typeArgument = langModelUtil.getGenericsTypes(fieldType).get(1);
            if (knownMessages.contains(typeArgument.toString()) ||
                    isProtoMessage(typeArgument) ||
                    isProtoEnum(typeArgument)) {
                return FieldType.MAP_TO_MESSAGE;
            } else {
                return FieldType.PRIMITIVE_MAP;
            }
        }

        return FieldType.OTHER;
    }

    public boolean isProtoMessage(TypeMirror fieldType) {
        if (fieldType.getKind().equals(TypeKind.DECLARED)) {
            return langModelUtil.getAnnotation(fieldType, ProtoClass.class) != null;
        }

        return false;
    }

    public boolean isProtoEnum(TypeMirror fieldType) {
        if (fieldType.getKind().equals(TypeKind.DECLARED)) {
            return langModelUtil.getAnnotation(fieldType, ProtoEnum.class) != null;
        }

        return false;
    }

    public TypeMirror getElementType(TypeMirror domainType, FieldType fieldType) {
        switch (fieldType) {
            case MESSAGE_LIST:
                if (domainType instanceof DeclaredType) {
                    DeclaredType declaredType = (DeclaredType) domainType;
                    return declaredType.getTypeArguments().get(0);
                }
                break;
            case MAP_TO_MESSAGE:
                if (domainType instanceof DeclaredType) {
                    DeclaredType declaredType = (DeclaredType) domainType;
                    return declaredType.getTypeArguments().get(1);
                }
                break;
        }
        return null;
    }

    public ProtoType getProtoTypeFromConverter(TypeMirror converter) {
        TypeMirror typeConverter = langModelUtil.getInterfaceOf(converter, TypeConverter.class.getName());
        List<? extends TypeMirror> typeArguments = langModelUtil.getGenericsTypes(typeConverter);
        TypeMirror protoType = typeArguments.get(1);

        if (langModelUtil.isMap(protoType)) {
            return ProtoType.MAP;
        }

        if (langModelUtil.isList(protoType)) {
            return ProtoType.LIST;
        }

        if (langModelUtil.isMessage(protoType)) {
            return ProtoType.MESSAGE;
        }

        return ProtoType.OTHER;
    }
}
