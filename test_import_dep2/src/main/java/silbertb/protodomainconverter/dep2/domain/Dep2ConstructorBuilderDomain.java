package silbertb.protodomainconverter.dep2.domain;

import lombok.Builder;
import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.model.proto.Dep2ConstructorBuilderProto;
import silbertb.protodomainconverter.dep1.domain.Dep1Domain;


@Data
@ProtoClass(protoClass = Dep2ConstructorBuilderProto.class)
public class Dep2ConstructorBuilderDomain {
    private final Dep1Domain dep1;

    @Builder
    @ProtoBuilder
    private Dep2ConstructorBuilderDomain(Dep1Domain dep1) {
        this.dep1 = dep1;
    }
}
