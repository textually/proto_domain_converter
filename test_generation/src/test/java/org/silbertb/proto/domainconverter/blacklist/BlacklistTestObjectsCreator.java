package org.silbertb.proto.domainconverter.blacklist;

import org.silbertb.proto.domainconverter.blacklist.domain.BlacklistDomain;
import org.silbertb.proto.domainconverter.blacklist.domain.oneof.domain_class.BlacklistOneofBaseDomain;
import org.silbertb.proto.domainconverter.blacklist.domain.oneof.domain_class.BlacklistOneofDoubleImplDomain;
import org.silbertb.proto.domainconverter.blacklist.domain.oneof.domain_field.*;
import org.silbertb.proto.domainconverter.test.proto.blacklist.*;

public class BlacklistTestObjectsCreator {
    public static BlacklistDomain createBlacklistDomain() {
        BlacklistDomain domain = new BlacklistDomain();
        domain.setIntValue("1");
        domain.setLongVal(2);
        domain.setDoubleValue(3.3);
        domain.setAnotherField("Another Value");

        return domain;
    }

    public static BlacklistProto createBlacklistProto() {
        return BlacklistProto.newBuilder()
                .setIntValue(1)
                .setLongValue(2)
                .setDoubleValue(3.3)
                .build();
    }

    public static BlacklistOneofFieldDomain createBlacklistOneofFieldStringDomain() {
        BlacklistOneofStringImpl stringImpl = new BlacklistOneofStringImpl();
        stringImpl.setStringValue("value");
        stringImpl.setAnotherValue(" another value");

        BlacklistOneofFieldDomain domain = new BlacklistOneofFieldDomain();
        domain.setBlacklistOneof(stringImpl);

        return domain;
    }

    public static BlacklistOneofFieldProto createBlacklistOneofFieldStringProto() {
        return BlacklistOneofFieldProto.newBuilder().setStringValue("value").build();
    }

    public static BlacklistOneofFieldDomain createBlacklistOneofFieldBoolDomain() {
        BlacklistOneofBoolImpl boolImpl = new BlacklistOneofBoolImpl();
        boolImpl.setBool(true);

        BlacklistOneofFieldDomain domain = new BlacklistOneofFieldDomain();
        domain.setBlacklistOneof(boolImpl);

        return domain;
    }

    public static BlacklistOneofFieldProto createBlacklistOneofFieldBoolProto() {
        return BlacklistOneofFieldProto.newBuilder().setBooleanValue(true).build();
    }

    public static BlacklistOneofFieldDomain createBlacklistOneofFieldBlacklistMsgDomain() {
        BlacklistOneofImplDomain msgImpl = new BlacklistOneofImplDomain();
        msgImpl.setStringValue("value");
        msgImpl.setAnotherValue(" another value");

        BlacklistOneofFieldDomain domain = new BlacklistOneofFieldDomain();
        domain.setBlacklistOneof(msgImpl);

        return domain;
    }

    public static BlacklistOneofFieldProto createBlacklistOneofFieldBlacklistMsgProto() {
        return BlacklistOneofFieldProto.newBuilder()
                .setBlacklistMsg(BlacklistOneofImplProto.newBuilder().setStringValue("value"))
                .build();
    }

    public static BlacklistOneofFieldDomain createBlacklistOneofFieldWhitelistMsgDomain() {
        WhitelistOneofImplDomain msgImpl = new WhitelistOneofImplDomain();
        msgImpl.setStr("value");
        msgImpl.setAnotherValue(" another value");

        BlacklistOneofFieldDomain domain = new BlacklistOneofFieldDomain();
        domain.setBlacklistOneof(msgImpl);

        return domain;
    }

    public static BlacklistOneofFieldProto createBlacklistOneofFieldWhitelistMsgProto() {
        return BlacklistOneofFieldProto.newBuilder()
                .setWhitelistMsg(WhitelistOneofImplProto.newBuilder().setStringValue("value"))
                .build();
    }

    public static BlacklistOneofFieldDomain createBlacklistOneofFieldInnerMsgDomain() {
        WhitelistOneofImplDomain msgImpl = new WhitelistOneofImplDomain();
        msgImpl.setStr("value");
        msgImpl.setAnotherValue(" another value");

        InnerMsgOneofImplDomain innerMsg = new InnerMsgOneofImplDomain();
        innerMsg.setInnerMsg(msgImpl);

        BlacklistOneofFieldDomain domain = new BlacklistOneofFieldDomain();
        domain.setBlacklistOneof(innerMsg);

        return domain;
    }

    public static BlacklistOneofFieldProto createBlacklistOneofFieldInnerMsgProto() {
        return BlacklistOneofFieldProto.newBuilder()
                .setInnerMsg(WhitelistOneofImplProto.newBuilder().setStringValue("value"))
                .build();
    }

    public static BlacklistOneofBaseDomain createBlacklistOneofBaseDomain() {
        BlacklistOneofDoubleImplDomain domain = new BlacklistOneofDoubleImplDomain();
        domain.setLongValue(1);
        domain.setDoubleValue(1.1);

        return domain;
    }

    public static BlacklistOneofBaseProto createBlacklistOneofBaseProto() {
        return BlacklistOneofBaseProto.newBuilder().setLongValue(1).setDoubleValue(1.1).build();
    }
}
