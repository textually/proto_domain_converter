package org.silbertb.proto.domainconverter.builder;

import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.basic.BasicTestObjectsCreator;
import org.silbertb.proto.domainconverter.builder.domain.*;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.builder.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class BuilderTest {
    @Test
    void testAllInOneBuilderToProto() {
        AllInOneBuilderDomain domain = BuilderTestObjectsCreator.createAllInOneBuilderDomain();
        AllInOneBuilderProto proto = ProtoDomainConverter.toProto(domain);
        AllInOneBuilderProto expected = BuilderTestObjectsCreator.createAllInOneBuilderProto();

        assertEquals(expected, proto);
    }

    @Test
    void testAllInOneBuilderToDomain() {
        AllInOneBuilderProto proto = BuilderTestObjectsCreator.createAllInOneBuilderProto();
        AllInOneBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        AllInOneBuilderDomain expected = BuilderTestObjectsCreator.createAllInOneBuilderDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomBuilderMethodToProto() {
        CustomBuilderMethodDomain domain = BuilderTestObjectsCreator.createCustomerBuilderMethodDomain();
        CustomBuilderMethodProto proto = ProtoDomainConverter.toProto(domain);
        CustomBuilderMethodProto expected = BuilderTestObjectsCreator.createCustomBuilderMethodProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomBuilderMethodToDomain() {
        CustomBuilderMethodProto proto = BuilderTestObjectsCreator.createCustomBuilderMethodProto();
        CustomBuilderMethodDomain domain = ProtoDomainConverter.toDomain(proto);
        CustomBuilderMethodDomain expected = BuilderTestObjectsCreator.createCustomerBuilderMethodDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomBuildMethodToProto() {
        CustomBuildMethodDomain domain = BuilderTestObjectsCreator.createCustomerBuildMethodDomain();
        CustomBuildMethodProto proto = ProtoDomainConverter.toProto(domain);
        CustomBuildMethodProto expected = BuilderTestObjectsCreator.createCustomBuildMethodProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomBuildMethodToDomain() {
        CustomBuildMethodProto proto = BuilderTestObjectsCreator.createCustomBuildMethodProto();
        CustomBuildMethodDomain domain = ProtoDomainConverter.toDomain(proto);
        CustomBuildMethodDomain expected = BuilderTestObjectsCreator.createCustomerBuildMethodDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomSetterPrefixToProto() {
        BuilderCustomSetterPrefixDomain domain = BuilderTestObjectsCreator.createBuilderCustomSetterPrefixDomain();
        BuilderCustomSetterPrefixProto proto = ProtoDomainConverter.toProto(domain);
        BuilderCustomSetterPrefixProto expected = BuilderTestObjectsCreator.createBuilderCustomSetterPrefixProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomSetterPrefixToDomain() {
        BuilderCustomSetterPrefixProto proto = BuilderTestObjectsCreator.createBuilderCustomSetterPrefixProto();
        BuilderCustomSetterPrefixDomain domain = ProtoDomainConverter.toDomain(proto);
        BuilderCustomSetterPrefixDomain expected = BuilderTestObjectsCreator.createBuilderCustomSetterPrefixDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testAllInOneBuilderConstructorToProto() {
        AllInOneBuilderConstructorDomain domain = BuilderTestObjectsCreator.createAllInOneBuilderConstructorDomain();
        AllInOneBuilderConstructorProto proto = ProtoDomainConverter.toProto(domain);
        AllInOneBuilderConstructorProto expected = BuilderTestObjectsCreator.createAllInOneBuilderConstructorProto();

        assertEquals(expected, proto);
    }

    @Test
    void testAllInOneBuilderToConstructorDomain() {
        AllInOneBuilderConstructorProto proto = BuilderTestObjectsCreator.createAllInOneBuilderConstructorProto();
        AllInOneBuilderConstructorDomain domain = ProtoDomainConverter.toDomain(proto);
        AllInOneBuilderConstructorDomain expected = BuilderTestObjectsCreator.createAllInOneBuilderConstructorDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testAllInOneBuilderToConstructorMixedWithSetterUpdateDomain() {
        AllInOneBuilderConstructorProto proto = BuilderTestObjectsCreator.createAllInOneBuilderConstructorProto();
        AllInOneBuilderConstructorDomain domain = createAllInOneBuilderConstructorForUpdate();
        //All the fields in 'domain' are different, and specifically strVal
        assertNotEquals(proto.getStrVal(), domain.getStrVal());

        AllInOneBuilderConstructorDomain domainResult =  ProtoDomainConverter.toDomain(proto, domain);
        assertSame(domain, domainResult);
        assertEquals(proto.getStrVal(), domain.getStrVal());

        AllInOneBuilderConstructorDomain domainCompleteConversion = ProtoDomainConverter.toDomain(proto);
        assertNotEquals(domainCompleteConversion, domain);
    }

    private AllInOneBuilderConstructorDomain createAllInOneBuilderConstructorForUpdate() {
        AllInOneBuilderConstructorDomain domain = AllInOneBuilderConstructorDomain.builder()
                .bytesVal(new byte[]{0x2b, 0x1b})
                .listVal(List.of("abc", "def"))
                .mapVal(new HashMap<>(Map.of("key11", "val11", "key22", "val22")))
                .value(BasicTestObjectsCreator.createPrimitiveDomain())
                .build();
        domain.setStrVal("123");
        return domain;
    }

}
