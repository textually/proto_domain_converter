package org.silbertb.proto.domainconverter.blacklist.domain.oneof.domain_field;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.blacklist.WhitelistOneofImplProto;

@Data
@ProtoClass(protoClass = WhitelistOneofImplProto.class)
public class WhitelistOneofImplDomain implements BlacklistOneof {

    @ProtoField(protoName = "string_value")
    private String str;

    @EqualsAndHashCode.Exclude
    private String anotherValue;
}
