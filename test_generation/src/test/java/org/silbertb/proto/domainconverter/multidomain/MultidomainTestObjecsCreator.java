package org.silbertb.proto.domainconverter.multidomain;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.silbertb.proto.domainconverter.basic.BasicTestObjectsCreator;
import org.silbertb.proto.domainconverter.multidomain.domain.Library;
import org.silbertb.proto.domainconverter.multidomain.domain.OneofHolder;
import org.silbertb.proto.domainconverter.multidomain.domain.filesystem.FileNode;
import org.silbertb.proto.domainconverter.test_util.FilesystemMarshal;
import org.silbertb.proto.domainconverter.test.proto.multidomain.FileNodeMsg;
import org.silbertb.proto.domainconverter.test.proto.multidomain.LibraryMsg;
import org.silbertb.proto.domainconverter.test.proto.multidomain.OneofHolderMsg;

import java.io.InputStream;
import java.io.InputStreamReader;

public class MultidomainTestObjecsCreator {
    @SneakyThrows
    public static LibraryMsg createLibraryProto() {
        LibraryMsg.Builder builder = LibraryMsg.newBuilder();
        parseProto(builder, "library.json");
        return builder.build();
    }

    @SneakyThrows
    public static Library createLibraryDomain() {
        Gson gson = new Gson();
        InputStream is = BasicTestObjectsCreator.class.getClassLoader().getResourceAsStream("library.json");
        assert is != null;
        return gson.fromJson(new InputStreamReader(is), Library.class);
    }

    public static OneofHolderMsg createOneofHolderProto(OneofHolderMsg.ValueCase valueCase) {
        final OneofHolderMsg.Builder builder = OneofHolderMsg.newBuilder();
        builder.setName("Name");
        switch (valueCase) {
            case X:
                builder.setX(OneofHolderMsg.A.newBuilder().setF(3.14159265359f).build());
                break;
            case Y:
                builder.setY(OneofHolderMsg.A.newBuilder().setF(2.71828182846f).build());
                break;
            case Z:
                builder.setZ(OneofHolderMsg.B.newBuilder().setI(1337).build());
                break;
            default:
                Assertions.fail("Unsupported value case: " + valueCase);
        }
        return builder.build();
    }

    public static OneofHolder createOneofHolderDomain(OneofHolderMsg.ValueCase valueCase) {
        final OneofHolder domain = new OneofHolder();
        domain.setName("Name");
        switch (valueCase) {
            case X:
                domain.setValue(new OneofHolder.X(3.14159265359f));
                break;
            case Y:
                domain.setValue(new OneofHolder.Y(2.71828182846f));
                break;
            case Z:
                domain.setValue(new OneofHolder.Z(1337));
                break;
            default:
                Assertions.fail("Unsupported value case: " + valueCase);
        }
        return domain;
    }

    public static FileNodeMsg createOneofBaseProto() {
        FileNodeMsg.Builder builder = FileNodeMsg.newBuilder();
        parseProto(builder, "filesystem_proto.json");
        return builder.build();
    }

    public static FileNode createOneofBaseDomain() {
        Gson gson = new GsonBuilder().registerTypeAdapter(FileNode.class, new FilesystemMarshal()).create();
        InputStream is = BasicTestObjectsCreator.class.getClassLoader().getResourceAsStream("filesystem_domain.json");
        assert is != null;
        return gson.fromJson(new InputStreamReader(is), FileNode.class);
    }

    @SneakyThrows
    static private void parseProto(Message.Builder builder, String fileName) {
        InputStream is = BasicTestObjectsCreator.class.getClassLoader().getResourceAsStream(fileName);
        assert is != null;
        JsonFormat.parser().ignoringUnknownFields().merge(new InputStreamReader(is), builder);
    }
}
