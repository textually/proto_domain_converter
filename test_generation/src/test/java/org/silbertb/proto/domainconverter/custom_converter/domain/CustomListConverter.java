package org.silbertb.proto.domainconverter.custom_converter.domain;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.CustomListConverterProto;

@Data
@ProtoClass(protoClass = CustomListConverterProto.class)
public class CustomListConverter {
    @ProtoField(protoName = "int_list")
    @ProtoConverter(converter = IntListToCommaSeparatedStringConverter.class)
    private String commaSeparatedInt;
}
