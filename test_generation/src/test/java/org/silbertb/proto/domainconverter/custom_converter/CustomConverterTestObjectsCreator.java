package org.silbertb.proto.domainconverter.custom_converter;

import org.silbertb.proto.domainconverter.custom_converter.domain.*;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.*;

import java.util.HashMap;
import java.util.UUID;

public class CustomConverterTestObjectsCreator {

    public static CustomConverter createCustomConverterDomain() {
        CustomConverter domain = new CustomConverter();
        domain.setStrVal("5");
        return domain;
    }

    public static CustomConverterProto createCustomConverterProto() {
        return CustomConverterProto.newBuilder().setIntVal(5).build();
    }

    public static CustomListConverter createCustomListConverterDomain() {
        CustomListConverter domain = new CustomListConverter();
        domain.setCommaSeparatedInt("5,6");
        return domain;
    }

    public static CustomListConverterProto createCustomListConverterProto() {
        return CustomListConverterProto.newBuilder().addIntList(5).addIntList(6).build();
    }

    public static CustomMapConverter createCustomMapConverterDomain() {
        CustomMapConverter domain = new CustomMapConverter();
        HashMap<String, String> map = new HashMap<>();
        map.put("1", "2");
        map.put("3", "4");

        domain.setMap(map);
        return domain;
    }

    public static CustomMapConverterProto createCustomMapConverterProto() {
        return CustomMapConverterProto.newBuilder().putIntMap(1, 2).putIntMap(3, 4).build();
    }

    public static MultiCustomConvertersDomain createMultiCustomConvertersDomain() {
        MultiCustomConvertersDomain domain = new MultiCustomConvertersDomain();
        domain.setStrVal("5");
        domain.setCommaSeparatedInt("5,6");
        HashMap<String, String> map = new HashMap<>();
        map.put("1", "2");
        map.put("3", "4");

        domain.setMap(map);

        return domain;
    }

    public static MultiCustomConvertersProto createMultiCustomConvertersProto() {
        return MultiCustomConvertersProto.newBuilder()
                .setIntVal(5)
                .addIntList(5).addIntList(6)
                .putIntMap(1, 2).putIntMap(3, 4)
                .build();
    }

    public static NullableFieldsExampleDomain createNullableFieldsExampleDomain() {
        final NullableFieldsExampleDomain domain = new NullableFieldsExampleDomain();
        domain.setId(UUID.fromString("4af504ee-2816-458c-9f81-99d766dc9fa2"));
        domain.setName("name");
        domain.setDescription("");
        domain.setAddress(null);
        domain.setAge(null);
        domain.setHeight(1.0);
        domain.setActive(true);
        domain.setWeight(null);
        return domain;
    }

    public static NullableFieldsExampleProto createNullableFieldsExampleProto() {
        return NullableFieldsExampleProto.newBuilder()
                .setId("4af504ee-2816-458c-9f81-99d766dc9fa2")
                .setName(StringWrapper.newBuilder().setValue("name").build())
                .setDescription(StringWrapper.newBuilder().setValue("").build())
                .setHeight(DoubleWrapper.newBuilder().setValue(1.0).build())
                .setActive(BooleanWrapper.newBuilder().setValue(true).build())
                .build();
    }
}
