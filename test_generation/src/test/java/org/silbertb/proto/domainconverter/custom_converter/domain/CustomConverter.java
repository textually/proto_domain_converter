package org.silbertb.proto.domainconverter.custom_converter.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.CustomConverterProto;

@Data
@ProtoClass(protoClass = CustomConverterProto.class)
public class CustomConverter {
    @ProtoField(protoName = "int_val")
    @ProtoConverter(converter = IntStrConverter.class)
    private String strVal;
}
