package org.silbertb.proto.domainconverter.multidomain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.multidomain.domain.Library;
import org.silbertb.proto.domainconverter.multidomain.domain.OneofHolder;
import org.silbertb.proto.domainconverter.multidomain.domain.filesystem.FileNode;
import org.silbertb.proto.domainconverter.test.proto.multidomain.FileNodeMsg;
import org.silbertb.proto.domainconverter.test.proto.multidomain.LibraryMsg;
import org.silbertb.proto.domainconverter.test.proto.multidomain.OneofHolderMsg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.assumeFalse;

public class MultidomainTest {
    @Test
    void testMultidomainToDomain() {
        LibraryMsg proto = MultidomainTestObjecsCreator.createLibraryProto();
        Library domain = ProtoDomainConverter.toDomain(proto);
        Library expected = MultidomainTestObjecsCreator.createLibraryDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMultidomainToProto() {
        Library domain = MultidomainTestObjecsCreator.createLibraryDomain();
        LibraryMsg proto = ProtoDomainConverter.toProto(domain);
        LibraryMsg expected = MultidomainTestObjecsCreator.createLibraryProto();

        assertEquals(expected, proto);
    }

    @ParameterizedTest(name = "{index} => valueCase=''{0}''")
    @EnumSource(OneofHolderMsg.ValueCase.class)
    void testMultidomainOneofFieldToDomain(OneofHolderMsg.ValueCase valueCase) {
        assumeFalse(valueCase == OneofHolderMsg.ValueCase.VALUE_NOT_SET);
        OneofHolderMsg proto = MultidomainTestObjecsCreator.createOneofHolderProto(valueCase);
        OneofHolder domain = ProtoDomainConverter.toDomain(proto);
        OneofHolder expected = MultidomainTestObjecsCreator.createOneofHolderDomain(valueCase);

        assertEquals(expected, domain);
    }

    @ParameterizedTest(name = "{index} => valueCase=''{0}''")
    @EnumSource(OneofHolderMsg.ValueCase.class)
    void testMultidomainOneofFieldToProto(OneofHolderMsg.ValueCase valueCase) {
        assumeFalse(valueCase == OneofHolderMsg.ValueCase.VALUE_NOT_SET);
        OneofHolder domain = MultidomainTestObjecsCreator.createOneofHolderDomain(valueCase);
        OneofHolderMsg proto = ProtoDomainConverter.toProto(domain);
        OneofHolderMsg expected = MultidomainTestObjecsCreator.createOneofHolderProto(valueCase);

        assertEquals(expected, proto);
    }

    @Test
    void testMultidomainOneofBaseToDomain() {
        FileNodeMsg proto = MultidomainTestObjecsCreator.createOneofBaseProto();
        FileNode domain = ProtoDomainConverter.toDomain(proto);
        FileNode expected = MultidomainTestObjecsCreator.createOneofBaseDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMultidomainOneofBaseToProto() {
        FileNode domain = MultidomainTestObjecsCreator.createOneofBaseDomain();
        FileNodeMsg proto = ProtoDomainConverter.toProto(domain);
        FileNodeMsg expected = MultidomainTestObjecsCreator.createOneofBaseProto();

        assertEquals(expected, proto);
    }
}
