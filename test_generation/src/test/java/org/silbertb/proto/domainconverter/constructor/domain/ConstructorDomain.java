package org.silbertb.proto.domainconverter.constructor.domain;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.test.proto.constructor.ConstructorProto;

@ProtoClass(protoClass = ConstructorProto.class)
@Data
public class ConstructorDomain {
    private final int intVal;
    private final String strVal;

    @ProtoConstructor
    public ConstructorDomain(int intVal, String strVal) {
        this.intVal = intVal;
        this.strVal = strVal;
    }
}
