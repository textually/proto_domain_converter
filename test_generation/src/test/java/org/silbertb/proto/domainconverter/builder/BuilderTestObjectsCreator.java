package org.silbertb.proto.domainconverter.builder;

import com.google.protobuf.ByteString;
import org.silbertb.proto.domainconverter.builder.domain.*;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofIntImplDomain;
import org.silbertb.proto.domainconverter.test.proto.builder.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BuilderTestObjectsCreator {
    public static AllInOneBuilderProto createAllInOneBuilderProto() {
        return AllInOneBuilderProto.newBuilder()
                .setBytesVal(ByteString.copyFrom(new byte[]{0x1b, 0x2b}))
                .setStrVal("ccc")
                .addAllListVal(List.of("aaa", "bbb"))
                .putAllMapVal(Map.of("key1", "val1", "key2", "val2"))
                .putMapVal("key2", "val2")
                .setOneofIntVal(3)
                .build();
    }

    public static AllInOneBuilderDomain createAllInOneBuilderDomain() {
        var domainBuilder = AllInOneBuilderDomain.builder()
                .bytesVal(new byte[]{0x1b, 0x2b})
                .strVal("ccc")
                .listVal(List.of("aaa", "bbb"))
                .mapVal(new HashMap<>(Map.of("key1", "val1", "key2", "val2")));

        OneofIntImplDomain oneofIntImplDomain = new OneofIntImplDomain();
        oneofIntImplDomain.setIntVal(3);

        return domainBuilder
                .value(oneofIntImplDomain)
                .build();
    }

    public static CustomBuilderMethodProto createCustomBuilderMethodProto() {
        return CustomBuilderMethodProto.newBuilder()
                .setIntVal(1)
                .build();
    }

    public static CustomBuilderMethodDomain createCustomerBuilderMethodDomain() {
        return CustomBuilderMethodDomain.toBuilder().intVal(1).build();
    }

    public static CustomBuildMethodProto createCustomBuildMethodProto() {
        return CustomBuildMethodProto.newBuilder()
                .setIntVal(1)
                .build();
    }

    public static CustomBuildMethodDomain createCustomerBuildMethodDomain() {
        return CustomBuildMethodDomain.builder().intVal(1).create();
    }

    public static BuilderCustomSetterPrefixProto createBuilderCustomSetterPrefixProto() {
        return BuilderCustomSetterPrefixProto.newBuilder()
                .setIntVal(1)
                .build();
    }

    public static BuilderCustomSetterPrefixDomain createBuilderCustomSetterPrefixDomain() {
        return BuilderCustomSetterPrefixDomain.builder().withIntVal(1).build();
    }

    public static AllInOneBuilderConstructorProto createAllInOneBuilderConstructorProto() {
        return AllInOneBuilderConstructorProto.newBuilder()
                .setBytesVal(ByteString.copyFrom(new byte[]{0x1b, 0x2b}))
                .setStrVal("ccc")
                .addAllListVal(List.of("aaa", "bbb"))
                .putAllMapVal(Map.of("key1", "val1", "key2", "val2"))
                .putMapVal("key2", "val2")
                .setOneofIntVal(3)
                .build();
    }

    public static AllInOneBuilderConstructorDomain createAllInOneBuilderConstructorDomain() {
        var domainBuilder = AllInOneBuilderConstructorDomain.builder()
                .bytesVal(new byte[]{0x1b, 0x2b})
                .listVal(List.of("aaa", "bbb"))
                .mapVal(new HashMap<>(Map.of("key1", "val1", "key2", "val2")));

        OneofIntImplDomain oneofIntImplDomain = new OneofIntImplDomain();
        oneofIntImplDomain.setIntVal(3);

        AllInOneBuilderConstructorDomain domain = domainBuilder
                .value(oneofIntImplDomain)
                .build();
        domain.setStrVal("ccc");

        return domain;
    }
}
