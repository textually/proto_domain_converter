package org.silbertb.proto.domainconverter.custom_converter.domain;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.MultiCustomConvertersProto;

import java.util.HashMap;

@Data
@ProtoClass(protoClass = MultiCustomConvertersProto.class)
public class MultiCustomConvertersDomain {

    @ProtoField(protoName = "int_val")
    @ProtoConverter(converter = IntStrConverter.class)
    private String strVal;

    @ProtoField(protoName = "int_list")
    @ProtoConverter(converter = IntListToCommaSeparatedStringConverter.class)
    private String commaSeparatedInt;

    @ProtoField(protoName = "int_map")
    @ProtoConverter(converter = IntMapStringMapConverter.class)
    private HashMap<String, String> map;
}
