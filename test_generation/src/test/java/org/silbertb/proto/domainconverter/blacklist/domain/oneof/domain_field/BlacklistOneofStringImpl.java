package org.silbertb.proto.domainconverter.blacklist.domain.oneof.domain_field;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class BlacklistOneofStringImpl implements BlacklistOneof{
    private String stringValue;

    @EqualsAndHashCode.Exclude
    private String anotherValue;
}
