package org.silbertb.proto.domainconverter.custom_mapper;

import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.custom_mapper.domain.CustomMapperDomain;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.custom_mapper.CustomMapperProto;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomMapperTest {
    @Test
    void testCustomMapperToDomain() {
        CustomMapperProto proto = CustomMapperDomainObjectsCreator.createCustomMapperProto();
        CustomMapperDomain domain = ProtoDomainConverter.toDomain(proto);
        CustomMapperDomain expected = CustomMapperDomainObjectsCreator.createCustomMapperDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomMapperToProto() {
        CustomMapperDomain domain = CustomMapperDomainObjectsCreator.createCustomMapperDomain();
        CustomMapperProto proto = ProtoDomainConverter.toProto(domain);
        CustomMapperProto expected = CustomMapperDomainObjectsCreator.createCustomMapperProto();

        assertEquals(expected, proto);
    }

}
