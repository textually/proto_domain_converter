package org.silbertb.proto.domainconverter.builder.domain;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.silbertb.proto.domainconverter.annotations.*;
import org.silbertb.proto.domainconverter.basic.domain.PrimitiveDomain;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofBaseFieldDomain;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofIntImplDomain;
import org.silbertb.proto.domainconverter.test.proto.builder.AllInOneBuilderProto;

import java.util.HashMap;
import java.util.List;

@ToString
@EqualsAndHashCode
@Builder
@ProtoBuilder
@ProtoClass(protoClass = AllInOneBuilderProto.class)
public class AllInOneBuilderDomain {
    @Getter
    @ProtoField
    private String strVal;

    @Getter
    @ProtoField
    byte[] bytesVal;

    @Getter
    @ProtoField
    HashMap<String, String> mapVal;

    @Getter
    @ProtoField
    private List<String> listVal;

    @Getter
    @OneofBase(oneOfFields = {
            @OneofField(protoField = "oneof_int_val", domainClass = OneofIntImplDomain.class, domainField = "intVal"),
            @OneofField(protoField = "oneof_primitives", domainClass = PrimitiveDomain.class)
    })
    private OneofBaseFieldDomain value;
}
