package org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain;

import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;

@Value
public class StringWrapperConstructor implements OneofFieldsBase {
    String strVal;

    @ProtoConstructor
    public StringWrapperConstructor(String strVal) {
        this.strVal = strVal;
    }
}
