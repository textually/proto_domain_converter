package org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain;

import lombok.Builder;
import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;

@ProtoBuilder
@Builder
@Value
public class StringWrapperBuilder implements OneofFieldsBase {
    String strVal;
}
