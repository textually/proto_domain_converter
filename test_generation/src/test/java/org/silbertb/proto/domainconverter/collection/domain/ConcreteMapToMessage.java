package org.silbertb.proto.domainconverter.collection.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;
import org.silbertb.proto.domainconverter.basic.domain.PrimitiveDomain;
import org.silbertb.proto.domainconverter.test.proto.collection.ConcreteMapToMessageProto;

import java.util.HashMap;

@Data
@ProtoClass(protoClass = ConcreteMapToMessageProto.class)
public class ConcreteMapToMessage {
    @ProtoField
    HashMap<String, PrimitiveDomain> mapToMessage;
}
