package org.silbertb.proto.domainconverter.test_util;

import com.google.gson.*;
import org.silbertb.proto.domainconverter.multidomain.domain.filesystem.File;
import org.silbertb.proto.domainconverter.multidomain.domain.filesystem.FileNode;
import org.silbertb.proto.domainconverter.multidomain.domain.filesystem.Folder;

import java.lang.reflect.Type;

public class FilesystemMarshal implements JsonDeserializer<FileNode> {

    @Override
    public FileNode deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        if (jsonObject.has("files")) {
            return context.deserialize(json, Folder.class);
        } else {
            return context.deserialize(json, File.class);
        }
    }
}
