package org.silbertb.proto.domainconverter.custom_converter.domain.wrapper;

import org.silbertb.proto.domainconverter.custom.TypeConverter;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.DoubleWrapper;

public class DoubleWrapperConverter implements TypeConverter<Double, DoubleWrapper> {
    @Override
    public Double toDomainValue(DoubleWrapper protoValue) {
        return protoValue.getValue();
    }

    @Override
    public boolean shouldAssignToProto(Double domainValue) {
        return domainValue != null;
    }

    @Override
    public DoubleWrapper toProtobufValue(Double domainValue) {
        return DoubleWrapper.newBuilder().setValue(domainValue).build();
    }
}