package org.silbertb.proto.domainconverter.custom_mapper.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoClassMapper;
import org.silbertb.proto.domainconverter.basic.domain.StringDomain;
import lombok.Data;
import org.silbertb.proto.domainconverter.test.proto.custom_mapper.CustomMapperProto;

@Data
@ProtoClassMapper(mapper = CustomMapper.class)
@ProtoClass(protoClass = CustomMapperProto.class)
public class CustomMapperDomain {
    private boolean isTcp;
    private boolean isUdp;
    private StringDomain stringDomain;

    public void setUdp(boolean udp) {
        isUdp = udp;
        isTcp = !udp;
    }

    public void setTcp(boolean tcp) {
        isTcp = tcp;
        isUdp = !tcp;
    }
}
