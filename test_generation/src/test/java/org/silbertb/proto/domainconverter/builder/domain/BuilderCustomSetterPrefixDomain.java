package org.silbertb.proto.domainconverter.builder.domain;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.builder.BuilderCustomSetterPrefixProto;

@ToString
@EqualsAndHashCode
@Builder(setterPrefix = "with")
@ProtoBuilder(setterPrefix = "with")
@ProtoClass(protoClass = BuilderCustomSetterPrefixProto.class)
public class BuilderCustomSetterPrefixDomain {
    @Getter
    @ProtoField
    private int intVal;
}
