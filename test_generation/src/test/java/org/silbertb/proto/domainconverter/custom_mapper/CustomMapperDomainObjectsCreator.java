package org.silbertb.proto.domainconverter.custom_mapper;

import org.silbertb.proto.domainconverter.custom_mapper.domain.CustomMapperDomain;
import org.silbertb.proto.domainconverter.basic.domain.StringDomain;
import org.silbertb.proto.domainconverter.test.proto.StringProto;
import org.silbertb.proto.domainconverter.test.proto.custom_mapper.CustomMapperProto;

public class CustomMapperDomainObjectsCreator {

    public static CustomMapperDomain createCustomMapperDomain() {
        CustomMapperDomain domain = new CustomMapperDomain();
        domain.setUdp(true);
        StringDomain stringDomain = new StringDomain();
        stringDomain.setStringValue("aaa");
        domain.setStringDomain(stringDomain);

        return domain;
    }

    public static CustomMapperProto createCustomMapperProto() {
        return CustomMapperProto.newBuilder()
                .setProtocol(CustomMapperProto.Protocol.UDP)
                .setStr(StringProto.newBuilder().setStringValue("aaa"))
                .build();
    }
}
