package org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain;

import lombok.Builder;
import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.*;
import org.silbertb.proto.domainconverter.test.proto.oneof.OneofDefinedFromFieldAnnotationBuilderProto;

@Builder
@Value
@ProtoBuilder
@ProtoClass(protoClass = OneofDefinedFromFieldAnnotationBuilderProto.class)
public class OneofDefinedFromFieldAnnotationBuilderDomain {

    @OneofBase(oneOfFields = {
            @OneofField(
                    protoField = "int_val",
                    domainClass = StringWrapperBuilder.class,
                    domainField = "strVal", converter = IntStrConverter.class)
    })
    OneofFieldsBase value;

}
