package org.silbertb.proto.domainconverter.collection.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;
import org.silbertb.proto.domainconverter.test.proto.collection.ConcretePrimitiveMapProto;

import java.util.TreeMap;

@Data
@ProtoClass(protoClass = ConcretePrimitiveMapProto.class)
public class ConcretePrimitiveMap {
    @ProtoField
    private TreeMap<Integer, Long> primitiveMap;
}
