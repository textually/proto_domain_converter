package org.silbertb.proto.domainconverter.custom_converter.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.CustomConverterConstructorProto;

@ToString
@EqualsAndHashCode
@ProtoClass(protoClass = CustomConverterConstructorProto.class)
public class CustomConverterConstructor {

    @ProtoConstructor
    public CustomConverterConstructor(
            @ProtoField(protoName = "int_val")
            @ProtoConverter(converter = IntStrConverter.class)
            String strVal) {
        this.strVal = strVal;
    }

    @Getter final private String strVal;
}
