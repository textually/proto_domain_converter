package org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain;

import org.silbertb.proto.domainconverter.custom.TypeConverter;

public class DoubleStringWrapperConstructorConverter implements TypeConverter<StringWrapperConstructor, Double> {
    @Override
    public StringWrapperConstructor toDomainValue(Double protoValue) {
        return new StringWrapperConstructor(protoValue.toString());
    }

    @Override
    public boolean shouldAssignToProto(StringWrapperConstructor domainValue) {
        return true;
    }

    @Override
    public Double toProtobufValue(StringWrapperConstructor domainValue) {
        return Double.valueOf(domainValue.getStrVal());
    }
}
