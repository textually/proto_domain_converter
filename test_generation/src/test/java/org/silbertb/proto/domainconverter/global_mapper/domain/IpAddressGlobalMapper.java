package org.silbertb.proto.domainconverter.global_mapper.domain;

import com.google.protobuf.ByteString;
import org.silbertb.proto.domainconverter.annotations.ProtoClassDefault;
import org.silbertb.proto.domainconverter.annotations.ProtoGlobalMapper;
import org.silbertb.proto.domainconverter.test.proto.IpAddressProto;

import java.net.Inet4Address;
import java.net.UnknownHostException;

@ProtoClassDefault
@ProtoGlobalMapper
public class IpAddressGlobalMapper implements InetIpAddressMapper {
    @Override
    public Inet4Address toDomain(IpAddressProto protoValue) {
        try {
            return (Inet4Address)Inet4Address.getByAddress(protoValue.getAddress().toByteArray());
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public IpAddressProto toProto(Inet4Address domainValue) {
        return IpAddressProto.newBuilder()
                .setAddress(ByteString.copyFrom(domainValue.getAddress()))
                .build();
    }
}
