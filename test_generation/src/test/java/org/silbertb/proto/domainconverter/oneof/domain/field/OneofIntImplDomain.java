package org.silbertb.proto.domainconverter.oneof.domain.field;

import lombok.Data;

@Data
public class OneofIntImplDomain implements OneofBaseFieldDomain {
    private int intVal;
}
